# TODO
sequelize
mongo
----------------------
*.d.ts
cors
jsonp : cb=fun fun('data')


## DONE
router + fs
wxopen 公众号开发
mysql cluster
redis pool   二级缓存
auth
node-cache 一级缓存
mysql2
tslint --init
is8n

##  注意
module 涉及到 this 只能到处整个
不要暴露和引用具体方法 防止this意外失效

## ab test and prof
yarn prof
ab -c 100 -n 10 -t 3 http://127.0.0.1:3000/
ctrl+c
node --prof-process .\isolate-0000013726B40300-31308-v8.log > profile.log