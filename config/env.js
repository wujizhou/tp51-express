const fs = require('fs')
const dotenv = require('dotenv')
const config = dotenv.config().parsed;
const ENV    = config.ENV
// console.log('process env config', process.env);
if (ENV){
  const envConfig = dotenv.parse(fs.readFileSync('.env.' + ENV))
  // ENV可能被覆盖
  for (const k in envConfig) {
    config[k] = envConfig[k]
    // 同时挂载到 process.env
    process.env[k] = envConfig[k]
  }
}

console.log(`======= app ${process.env.APP} run in mode  : ${ENV}`);
// 使用最开始的ENV
const env = {
  ENV,
  PORT: parseInt(config.PORT),
  APP: config.APP,
  VERSION : config.VERSION,
  LOG: {
    FILE: config.LOG_FILE_PATH,
    LEVEL: ENV === 'prod' ? 'debug' : 'info',
    PREFIX: 'tp51',
  },
  UPLOAD: {
    PATH: config.UPLOAD_PATH
  },
  MYSQL : {
    HOST : config.MYSQL_HOST,
    USER : config.MYSQL_USER,
    PREFIX : config.MYSQL_PREFIX || 'f_',
    PASS : config.MYSQL_PASS,
    PORT: parseInt(config.MYSQL_PORT) || 3306,
    LIMIT: parseInt(config.MYSQL_CONNECTION_LIMIT) || 5,
    DB : config.MYSQL_DB,
  },
  MYSQL_SLAVE : {
    HOST : config.MYSQL_SLAVE_HOST,
    USER : config.MYSQL_SLAVE_USER,
    PREFIX: config.MYSQL_PREFIX || 'f_',
    PASS : config.MYSQL_SLAVE_PASS,
    PORT: parseInt(config.MYSQL_SLAVE_PORT) || 3306,
    LIMIT: parseInt(config.MYSQL_CONNECTION_LIMIT) || 5,
    DB : config.MYSQL_SLAVE_DB,
  },
  MONGO: {
    HOST: config.MONGO_HOST,
    PORT: parseInt(config.MONGO_PORT) || 27017,
    USER: config.MONGO_USER,
    PASS: config.MONGO_PASS,
    DB: config.MONGO_DB,
    REPLICA: '',
    SSL_CA_PATH: '',
    READ_PREFERENCE:'',
    RETRY_WRITES: '',
  },
  REDIS: {
    // OPEN: config.REDIS_OPEN ? true: false ,
    HOST: config.REDIS_HOST,
    PORT: parseInt(config.REDIS_PORT) || 6379,
    PASS: config.REDIS_PASS,
    DB: config.REDIS_DB,
  }
}
module.exports = env
module.exports.env = env;
module.exports.default = env;