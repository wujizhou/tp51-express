const logger    = require('../utils/log').logger;
const rspHelper = require('../utils/rsp');
const Curl      = require('../utils/axios');
// const Curl      = require('../utils/fetch');

exports.getStatus = async (req, rsp) => {
  logger.log({
    level: 'info',
    message: 'Checking server health'
  });
  return rspHelper(rsp).ok();
}
exports.getHealth = (req, rsp) => {
  return rspHelper(rsp).ok();
}

exports.curl = async (req, rsp) => {
  const url = req.body.url
  const ret = {
    body: req.body,
    query: req.query,
    param: req.param,
  }
  ret.data = await Curl.post(url)
  return rspHelper(rsp).suc(ret);
}