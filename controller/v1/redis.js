/*
 * @Author: rainbow
 * @Date: 2021-09-Sa 08:10:54
 * @Last Modified by:   rainbow
 * @Last Modified time: 2021-09-Sa 08:10:54
 */
const rspHelper = require('../../utils/rsp')
const Cache  = require('../../db/cache')
const Redis  = require('../../db/redis')
// const O  = require('../../utils/O')
// const { log } = require('winston')

module.exports = {
  setCache: (req, rsp) => {
    const {key,val} = req.body
    const conf = { key:'obj' ,val:{ a:2, b:3}}
    Cache.mset([ {key,val}, conf ])
    return rspHelper(rsp).suc(Cache.set(key, val));
  },
  getCache: (req, rsp) => {
    return rspHelper(rsp).suc(Cache.get(req.body.key));
  },
  set: async (req, rsp) => {
    const ret = {}
    const {method,key,val} = req.body
    // const conn = await Redis.getConn() // Redis.Redis
    // const conn = await Redis.getConn(true) // OK
    // const conn = await Redis.getConn(true,true) // Redis.Pipeline
    const conn = await Redis.getConn()
    if (method == 'setbit'){
      ret[method+'PreBit'] = await Redis.setbit('sign:1', key,val)
      // ret[method + 'PreBit'] = await Redis.setbitByConn(conn, 'sign:1', key,val)
    } else if (method == 'getbit'){
      ret[method] = await Redis.getbit('sign:1', key)
      // ret[method] = await Redis.getbitByConn(conn,'sign:1', key)
    } else if (method == 'bitcount'){
      ret[method] = await Redis.bitcount('sign:1', key,val)
      // ret[method] = await Redis.bitcountByConn(conn,'sign:1', key,val)
    } else if (method == 'set'){
      ret[method] = await Redis.set( key,val)
      // ret[method] = await Redis.setByConn(conn, key,val)
    } else if (method == 'execp'){
      ret[method] = await Redis.pipeline([['set','u',11],['get','u']])
      // ret[method] = await Redis.pipelineByConn(conn,[['set','u',1],['get','u']])
    } else if (method == 'execm'){
      ret[method] = await Redis.multi([['set','u',22],['get','u']])
      // ret[method] = await Redis.multiByConn(conn,[['set','u',2],['get','u']])
    } else if (method == 'get'){
      // ret[method] = await Redis.get( key,true)
      ret[method] = await Redis.getByConn(conn, key, true)
    }
    Redis.end(conn)
    return rspHelper(rsp).suc(ret);
  }
}