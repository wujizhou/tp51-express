const rspHelper = require('../../utils/rsp')
const {afterUpload:afterUp} = require('../../utils/upload')
const api = require('../../db/api/userApi');

module.exports = {
  create: (req, rsp) => {
    return rspHelper(rsp).suc({msg:'success'});
  },
  user:  async (req, rsp) => {
    const id   = req.params.id
    const user = await api.getById(id)
    if(!user){
      // ... user not exist
    }
    const ret = {
      id,
      query: req.query,
      body: req.body,
      user: user
    }
    return rspHelper(rsp).suc(ret)
  },
  query:  async (req, rsp) => {
    const page = req.query.page || 1
    const size = req.query.size || 10
    const ret = {
      query: req.query,
      body: req.body
    }
    const { count, list } = await api.queryCount(page,size)
    ret.count = count
    ret.list  = list
    // res.list = await api.query(page,size)
    return rspHelper(rsp).suc(ret)
  },
  add:  async (req, rsp) => {
    const id = await api.add({
      name: 'tt313',
      nick: 'tt2',
      pass: '123456',
      email: '777',
      phone: 'tt2',
    })
    return rspHelper(rsp).suc(id)
  },
  upd: async (req,rsp) => {
    const user = {
      name: 'test',
      nick: 'tt2',
      pass: '123456',
      email: '777',
      phone: 'tt2',
    }
    const ret = await api.upd(user,{id:12})
    // user.id = 12
    // const res = await api.updById(user)
    return rspHelper(rsp).suc(ret)
  },
  del: async (req,rsp) => {
    // const res = await api.del({id:15,name:"tt3"})
    const res = await api.delById(17)
    return rspHelper(rsp).suc(res)
  },
  upload: (req, rsp) => {
    const ret = {
      id: req.params.id,
      query: req.query,
      body: req.body,
      file: req.file,
      files: req.files,
    }
    afterUp(req.file)
    return rspHelper(rsp).suc(ret)
  },
}