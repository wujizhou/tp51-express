/*
 * @Author: rainbow
 * @Date: 2021-09-Sa 08:10:36
 * @Last Modified by:   rainbow
 * @Last Modified time: 2021-09-Sa 08:10:36
 */
const NodeCache = require("node-cache")
// const {env}     = require("../config")
// 单机内存缓存
// 默认2分钟 检查删除10分钟
const Cache = new NodeCache({ stdTTL: 120, checkperiod: 600 });
// set(k,v,ttl?)
// has(k)
// take(k)  =  get(k) + del(k)
// mset(Array<{key, val, ttl?}>)
// mget([key1, key2, ..., keyn])
// del(k)
// del([key1, key2, ..., keyn])
// ttl(key, ttl)  重新定义有效期 ttl<0=del
// getTtl(key)  重新定义有效期 ttl<0=del
// keys()
// getStats()   数据
// flushAll()
// flushStats()
// close()

// 进程退出时自动关闭连接池
// process.on('exit', (code) => {
//   console.log('==== close node-cache',code);
//   Cache.close()
// })

module.exports = Cache