const { Model, DataTypes,Op } = require("sequelize");
const orm = require('../orm')
const { gzipSync, gunzipSync } = require('zlib');

const tableName = 'f_a_user'
class User extends Model { }
User.init({
  name: {
    type: DataTypes.STRING(32),
    allowNull: false,
    unique: true,
    get() {
      const rawValue = this.getDataValue('name');
      return rawValue ? rawValue.toUpperCase() : null;
    }
  }
  , pass: {
    type: DataTypes.STRING
    ,is: /^[0-9a-f]{64}$/i
    ,set(value) {
      // bcrypt()
      this.setDataValue('password', hash(value));
    }
  }
  ,email: {
    type: DataTypes.STRING(64),
    isEmail: true,
  }
  ,emailAuth: {
    type: DataTypes.BOOLEAN
    ,defaultValue: false
  }
  ,nick: DataTypes.STRING(32)
  ,pass: DataTypes.STRING(96)
  ,content: {
    type: DataTypes.TEXT,
    get() {
      const storedValue = this.getDataValue('content');
      if (storedValue){
        const gzippedBuffer = Buffer.from(storedValue, 'base64');
        const unzippedBuffer = gunzipSync(gzippedBuffer);
        return unzippedBuffer.toString();
      }
      return storedValue
    },
    set(value) {
      if(value){
        const gzippedBuffer = gzipSync(value);
        this.setDataValue('content', gzippedBuffer.toString('base64'));
      }
    }
  }
  ,testTime: { type: DataTypes.DATE, defaultValue: DataTypes.NOW }

  ,fullName: {
    type: DataTypes.VIRTUAL,
    get() {
      return `${this.firstName} ${this.lastName}`;
    },
    set(value) {
      throw new Error('不要尝试设置 `fullName` 的值!');
    }
  }
}, {
  sequelize: orm,
  tableName
});

// // test only
// (async () => {
//  await User.sync({ alter: true });// {force: true,alter:true,match: /_test$/}
//   // add
//   // const user = await User.create({name: 'testUser'})
//   // await user.destroy()

//   // const user = await User.findByPk(1)
//   // const user = await User.findOne({ where: {name:'newName'}})
//   // user.name = 'oldName'
//   // await user.save({ fields: ['name']})

//   const { count, rows } = await User.findAndCountAll({
//     attributes: ['id',['name','newName']]
//     // ,where: {
//     //   [Op.and]: [
//     //   { id:1}
//     //   ,{name: {
//     //       [Op.like]: 'old%'
//     //     }}
//     //   ]
//     // },
//     ,where: {
//       id:1
//       ,name: {
//         [Op.like]: 'old%'
//       }
//     },
//     offset: 0,
//     limit: 2
//   });
//   console.log(count, JSON.stringify(rows, null, 2), rows[0].getDataValue('newName'))
//   const [results, metadata] = await orm.query("UPDATE users SET y = 42 WHERE x = 12");
//    const users = await orm.query("SELECT * FROM `users`", {
//  type: QueryTypes.SELECT });
// const user = await orm.query(
//   'SELECT * FROM f_user WHERE status = ?',
//   {
//     model: User,
//     mapToModel: true
//     plain: true, // 返回第一条
//     replacements: ['active'],
//     type: QueryTypes.SELECT
//   }
// );
// })();

module.exports = User