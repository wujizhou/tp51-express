const Sequelize = require('sequelize');
const env = require('../config')

const orm = new Sequelize(env.MYSQL.DB, env.MYSQL.USER, env.MYSQL.PASS, {
  host: env.MYSQL.HOST,
  dialect: 'mysql',/* 选择 'mysql' | 'mariadb' | 'postgres' | 'mssql' 其一 */
});

module.exports = orm