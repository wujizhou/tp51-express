const { env } = require('../../config')
const O   = require('../../utils/O')
const Sql = require('../../utils/Sql');
const UserSql = Object.assign({}, Sql, { TABLE: env.MYSQL.PREFIX + 'user' })
// other user handle
// 如果敏感判断事务里for update
// 里面有this 必须fuction() {}

/**
 *  if name has used by other
 * @param   <user>{name:string,id?:number}</user>
 * @return  {sql:string,value:any[]}
 */
UserSql.isNameExist = function (user) {
  O.isObject(user,'')
  O.isString(user.name, 'user must have a name')
  if(user.id){
    return {
      sql: this.selectOne('id', 'name=? and id !=?')
      ,value: [user.name,user.id]
    }
  }else{
    return {
      sql: this.selectOne('id', 'name=?')
      ,value: [user.name]
    }
  }
}

module.exports = UserSql