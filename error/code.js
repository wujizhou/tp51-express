'use strict';
// CustomError : code

const STATUS = {
  SUCCESS: 'success',
  FAIL: 'fail'
}

const HTTP_STATUS = {
  /**
   * 200
   */
  OK: 200,
  /**
   * 202
   */
  ACCEPTED: 202,
  /**
   * 303
   */
  REDIRECT: 303,
  /**
   * 400
   */
  BAD_REQUEST: 400,
  /**
   * 401
   */
  UNAUTHORIZED: 401,
  /**
   * 403
   */
  FORBIDDEN: 403,
  /**
   * 404
   */
  NOT_FOUND: 404,
  /**
   * 406
   */
  NOT_ACCEPTABLE: 406,
  /**
   * 408
   */
  REQUEST_TIMEOUT: 408,
  /**
   * 422
   */
  UNPROCESSABLE_ENTITY: 422,
  /**
   * 500
   */
  INTERNAL_SERVER_ERROR: 500,
  /**
   * 601
   */
  UNDEFINED_ERROR: 601,
  /**
   * 602
   */
  UNDEFINED_VAR : 602,
}

const ERROR_CODE = Object.freeze({
  SUCCESS: {
    code: HTTP_STATUS.OK,
    msg: STATUS.SUCCESS,
  },
  BAD_REQUEST: {
    code: HTTP_STATUS.BAD_REQUEST,
    msg: 'bad request',
  },
  UNAUTHORIZED: {
    code: HTTP_STATUS.UNAUTHORIZED,
    msg: 'unauthorized',
  },
  DUPLICATE_REQUEST: {
    code: HTTP_STATUS.BAD_REQUEST,
    msg: 'duplicate request',
  },
  UNKNOWN_ERROR: {
    code: HTTP_STATUS.INTERNAL_SERVER_ERROR,
    msg: 'unknown error',
  },
  UNDEFINED_ERROR: {
    code: HTTP_STATUS.UNDEFINED_ERROR,
    msg: 'undefined error',
  },
  UNDEFIEND_VAR: {
    code: HTTP_STATUS.UNDEFIEND_VAR,
    msg: 'undefined var',
  },
  INVALID_HEADER: {
    code: HTTP_STATUS.BAD_REQUEST,
    msg: 'invalid header',
  },
  NOT_FOUND: {
    code: HTTP_STATUS.NOT_FOUND,
    msg: 'not found',
  },
  INTERNAL_SERVER_ERROR: {
    code: HTTP_STATUS.INTERNAL_SERVER_ERROR,
    msg: 'internal server error',
  },
})

module.exports = {
  ERROR_CODE,
  STATUS,
  HTTP_STATUS
};
