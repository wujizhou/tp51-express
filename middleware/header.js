const { ERROR_CODE,CustomError } = require('../error');
const common = require('../utils/common');
// const { logger } = require('../utils/logger');

const checkHeader = async (req, res, next) => {
  try {
    // multipart/form-data; boundary=--------------------------866783608390718706076417
    // application/x-www-form-urlencoded
    // text/plain  text/html
    // application/json application/xml
    // image/jpeg image/png
    const content_type = req.headers['content-type'];
    console.log('== middleware == ', content_type);
    if (!common.isEmpty(content_type) && content_type.toLowerCase() === 'application/json') {
      return next();
    } else {
      const error = new CustomError(ERROR_CODE.INVALID_HEADER.msg, ERROR_CODE.INVALID_HEADER.code);
      return next(error);
    }
    return next();
  } catch (e) {
    next(e);
  }
};

module.exports = checkHeader