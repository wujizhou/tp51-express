'use strict';

const router = require('express').Router()
const { getStatus, getHealth, curl } = require('../controller/health')
const O = require('../utils/O');

router
  .get('/', O.wrapRouter(getStatus))
  .get('/health', O.wrapRouter(getHealth))
  .post('/curl', O.wrapRouter(curl))
  ;

// 一定要 modul.exports
module.exports = router