// 'use strict';

// const health = require('./health');
// const v1 = require('./v1');

// // 一定要 modul.exports
// module.exports = (app) => {
//   app.use('/v1', health);
//   app.use('/v1', v1);
// }

const fs = require('fs')
const basePath = __dirname
let routers = []
fs.readdirSync(basePath).forEach(file => {
  if (!file.startsWith('index')) {
    // console.log('file', basePath+ '\\' +file);
    routers.push(require(basePath + '\\' + file));
  }
})
// 一定要 modul.exports
module.exports = (app) => {
  routers.forEach(r => {
    app.use('/v1', r)
  })
}