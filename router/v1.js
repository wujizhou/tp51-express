const router = require('express').Router();
const xmlParser   = require('express-xml-bodyparser');
// var xml2jsDefaults = {
//   explicitArray: false,
//   normalize: false,
//   normalizeTags: false,
//   trim: true
// }
// const auth    = require('../middleware');

const user   = require('../controller/v1/user');
const mongo  = require('../controller/v1/mongo');
const redis  = require('../controller/v1/redis');
const wx     = require('../controller/v1/wx');
const O       = require('../utils/O');
const { imgUpload: upload } = require('../utils/upload');

// params  :id
// query   ?id=id
// body    body-parser raw/json/..
//         multer form-data
// 路由函数 拿到函数的引用导致 this丢失
//  wx.handleUserMsg.bind(wx) 或不使用 this
router
.get('/wxMenu', O.wrapRouter(wx.createMenu))
  .get('/wxOpen', O.wrapRouter(wx.checkServer))
  .post('/wxOpen', xmlParser({ trim: false, explicitArray: false }),O.wrapRouter(wx.handleUserMsg))

.post('/mongo', O.wrapRouter(mongo.set))
.post('/redis', O.wrapRouter(redis.set))
.get('/cache', O.wrapRouter(redis.getCache))
  .post('/cache', O.wrapRouter(redis.setCache))

.get('/test/:id', O.wrapRouter(user.create))

.get('/user/:id', O.wrapRouter(user.user))
  .post('/userAdd', O.wrapRouter(user.add))
  .post('/userDel', O.wrapRouter(user.del))
  .post('/userUpd', O.wrapRouter(user.upd))
  .get('/userList', O.wrapRouter(user.query))

.post('/upload/:id', upload.single('avatar'), O.wrapRouter(user.upload))
;

// 必须 module.exports
module.exports = router;