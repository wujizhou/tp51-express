const orm = require('../../db/orm')
const testConnect = async () => {
  try {
    await orm.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }finally{
    orm.close()
  }
}
// test connect
(async () => await testConnect())()