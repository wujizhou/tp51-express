// is8n

const L = function (k, LANG = '',split = ' '){
  if (LANG == 'zh') {
    return k.split(split).filter(i=>i).map(i => LL(i,LANG)).join('')
  }else{
    return k.split(split).filter(i=>i).map(i=> LL(i,LANG)).join(' ')
  }
}
const LL = function (k, LANG = ''){
  return (Lang && Lang[LANG] && Lang[LANG][k]) ? Lang[LANG][k] : k
}
const Lang = {
  'zh': {
    err: '错误',
    err_type_invalid: 'type err {msg}',
    exist: '存在',
    has: '已经',
    invalid: '非法',
    ok: '成功',
    type: '类型',
  },
}
module.exports = L