'use strict';

module.exports = {
    INQUIRE_TYPE:['transaction_id', 'reference'],
    TRANSACTION_TYPE:['charge', 'capture', 'refund', 'cancel','chargeback'],
    ROUTE_MAP: {
        charges: "charge",
        capture: "capture",
        refunds: "refund",
        transactions: "inquiry",
        cancels: "refund",
        vault: "vault",
        installments: "installment",
        config: "config",
    },
    ALTERNATIVE_PAYMENT_METHOD:['card','paypal', 'venmo'],
    TRANS_STATUS: {
        SUCCESS: 'succeeded',
        FAIL: 'failed',
        INIT: 'initiated',
        AUTH: 'authorized',
        PENDING: 'pending',
        CAPTURED: 'captured',
        CANCEL: 'canceled',
        EXPIRED: 'expired'
    },
    TIME_CONSTANT: {
        GATEWAY_TIMEOUT: 5000,
        CHARGETOKEN_EXPIRY: 86400000
    }
};
