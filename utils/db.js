const O = require('./O')

const Db = {
  getCount (mysql2Ret) {
    const obj = mysql2Ret[0]
    return obj.length ? obj[0]['c'] : 0
  },
  getRowWithIdOrFalse (mysql2Ret) {
    const obj = mysql2Ret[0][0]
    return (obj && obj.id) ? obj : false
  },
  getRows (mysql2Ret) {
    return mysql2Ret[0]
  },
    // [{  "fieldCount": 0,
    // "affectedRows": 1,
    //   "insertId": 13,
    //     "info": "",
    //       "serverStatus": 3,
    //         "warningStatus": 5},undefiend]
  getInsertId (mysql2Ret) {
    return mysql2Ret[0]['insertId']
  },
  // [
  //   {
  //     "fieldCount": 0,
  //     "affectedRows": 1,
  //     "insertId": 0,
  //     "info": "",
  //     "serverStatus": 2,
  //     "warningStatus": 0
  //   },
  //   null
  // ]
  getAffects (mysql2Ret) {
    return mysql2Ret[0]['affectedRows']
  },
  /** page: df:1  1+
    * size: df:10 1~100
    * return limitSql|string
    */
  getLimit(page = 1, size = 10) {
    page = Math.max(1, page)
    size = Math.min(Math.max(1, size), 100)
    return ` limit ${(page - 1) * size},${size} `
  },


  /**
  * pre      df:'where' 是否条件前需要加 pre
  * whereArr 条件数组[string]
  * join     条件怎么拼接
  * side     是否需要加 ( )
  * return whereSql|string
  */
  getWhereSql(pre = 'where', whereArr, join = 'and', side = false) {
    let whereSql = '';
    if (whereArr && whereArr.length > 0) {
      whereSql = ` ${pre} ${side ? ' ( ' : ' '} ${whereArr.join(' ' + join + ' ')} ${side ? ' ) ' : ' '}`;
    }
    return whereSql
  },
  /**
   *
   *  Object.assign(setObj.values,whereObj.values)
   *  upd慎用 : 因为 values 会覆盖set的
   * return setObj|{sql,values}
   *        sql:    预处理字符串
   *        values: 变量数组
   */
  // TODO
  getWhereObj(pre = 'where', obj, join = 'and', side = false) {
    throw new Error('TODO')
    // let dataKey = [], dataValue = [];
    // let whereSql = '';
    // for (let k in obj) {
    //   if (typeof obj[k] !== 'function') {
    //     // whereSql = ` ${pre} ${side ? ' ( ' : ' '} ${whereArr.join(' ' + join + ' ')} ${side ? ' ) ' : ' '}`;
    //   }
    // }
    // return whereSql
  },
  getWhere(whereArr) {
    return this.getWhereSql('where', whereArr)
  },

  /**
  * Object.assign(setObj.values,whereObj.values)
  * return setObj|{sql,values}
  * sql:    预处理字符串
  * values: 变量数组
  */
  getSetObj(obj) {
    let dataKey = [], dataValue = [];
    for (let k in obj) {
      if (O.isString(obj[k])) {
        dataKey.push([`\`${k}\``, "?"].join(" = "));
        dataValue.push(obj[k]);
      } else if (O.isNumber(obj[k])) {
        dataKey.push([`\`${k}\``, "?"].join(" = "));
        dataValue.push(`'${obj[k]}'`);
      }
    }
    O.isArrayEmp(dataKey)
    const sql = ` set ${dataKey.join(', ')}`
    return { sql, values: dataValue }
  },
  getSetSql(obj) {
    let dataKey = []
    for (let k in obj) {
      if (O.isString(obj[k])) {
        dataKey.push([`\`${k}\``, `'${obj[k]}'`].join(" = "));
      } else if (O.isNumber(obj[k])) {
        dataKey.push([`\`${k}\``, obj[k]].join(" = "));
      }
    }
    return ` set ${dataKey.join(', ')}`
  },
}

// 不要暴露和引用具体方法 防止this意外失效
module.exports = Db