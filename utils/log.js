'use strict';

const winston = require('winston');
const expressWinston = require('express-winston');
require('winston-daily-rotate-file');
const message = Symbol.for('message');
const path = require('path');

const { env } = require('../config');

const jsonFormatter = (logEntry) => {
  const base = { timestamp: new Date(), path: path.basename(__dirname) + '/' + path.basename(__filename, '.js') };
  const json = Object.assign(base, logEntry);
  logEntry[message] = JSON.stringify(json);
  return logEntry;
};

const transport = new (winston.transports.DailyRotateFile)({
  level: env.LOG.LEVEL,
  format: winston.format(jsonFormatter)(),
  filename: env.LOG.FILE + `/${env.LOG.PREFIX}-%DATE%.log`,
  datePattern: 'YYYYMMDD',
  zippedArchive: false,
  maxSize: '20m',
  maxFiles: '180d'
});

transport.on('rotate', (oldFilename, newFilename) => {
  // do something fun
});

const logger = winston.createLogger({
  transports: [
    transport
  ],
  // format: winston.format.combine(
  //     winston.format.timestamp(),
  //     winston.format.colorize(),
  //     winston.format.json()
  // ),
});
exports.logger = logger
exports.expLogger = expressWinston.logger({
  winstonInstance: logger,
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: (req, res) => {
    const paths = [
      "/health"
    ];

    return paths.indexOf(req.url) >= 0;
  } // optional: allows to skip some log messages based on request and/or response
});