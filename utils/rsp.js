'use strict';

const { env } = require('../config');
const L = require('../utils/L');
const { HTTP_STATUS, STATUS,CustomError, ERROR_CODE, DbError } = require('../error');
const { logger } = require('../utils/log');

const { APP = '',ENV='', VERSION = '' } = env;
// const isProd = true
const isProd = ENV == 'prod'
const rspHelper = (rsp) => {

    const base = (code, data = {}, msg='',lang='') => {
        if(typeof code !== 'number') msg = `return code must be number, but get: ${code}`
        if(typeof msg !== 'string')  msg = `return msg must be string, but get: ${msg}`
        let obj = { app: APP, version: VERSION, code };
        // if (typeof data=='string') {
        //     // console.log('get err string ',data);
        //     obj.msg  = lang ? L(data,lang) : data;
        //     obj.data = {};
        // }else{
            obj.msg  = lang ? L(msg) : msg;
            obj.data = data;
        // }
        if(!isProd) obj.lang = lang
        return obj;
    }

    return {
        ok: () => {
             return rsp.json(base(HTTP_STATUS.OK));
        },
        suc: (data,msg=STATUS.SUCCESS) => {
            return rsp.json(base(HTTP_STATUS.OK, data,msg));
        },
        err: (err) => {
            try{
                if (!(err instanceof Error)) {
                    const lang = err.lang
                    err = new CustomError(ERROR_CODE.UNDEFINED_ERROR.msg,ERROR_CODE.UNDEFINED_ERROR.code);
                    err.lang = lang
                }
                // 错误本身报错:ERROR实现类 导致自定义的code 可能没有值
                if(!err.code) err.code = 500
                // 特定错误简单返回
                if (err instanceof CustomError && err.code === CustomError.CODE + HTTP_STATUS.NOT_FOUND) {
                    // 404 简单记录和返回
                    logger.error({
                        code: err.code,
                        msg: err.message,
                    });
                    return rsp.json(base(err.code, {}, err.message,err.lang));
                } else if (err instanceof DbError) {
                    if(err.detail){
                        logger.error({
                            code: err.code,
                            msg: err.detail,
                        });
                        // Error{message,code, errno,sql,sqlState,sqlMessage}
                        return rsp.json(base(err.code, isProd ? {} : { env: ENV, detail: err.detail }, err.message,err.lang));
                    }else{
                        logger.error({
                            code: err.code,
                            msg: err.message,
                        });
                        return rsp.json(base(err.code, {}, err.message,err.lang));
                    }
                } else {
                    logger.error({
                        code: err.code,
                        msg: err.message,
                        stack: err.stack
                    });
                    return rsp.json(base(err.code, isProd ? {} : {env:ENV,detail: err.stack}, err.name +':'+ err.message,err.lang));
                }
            }catch(e){
                // 未知系统错误
                console.log("==== server err", e)
                return rsp.json({ app: APP, version: VERSION, code: 500, data: isProd ? {} : { env: ENV, detail: err.stack },msg:'server error'})
            }
        }
    }

}

module.exports = rspHelper;

